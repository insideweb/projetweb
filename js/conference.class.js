
var newCollection = new Array();
var oldCollection = new Array();
var LesConferences = new Array();
var compteur = 0;
var compteurdate = 0;
var page = 1;

RemplirCollection();
Gererpagination(1,1); //Fonction d'initialisation avec les parametre 1,1

$(document).ready(function() {
      CreatePagination(); //Création de la Pagination
    $(document.getElementById("1")).addClass("active");
    //Mise en place d'événements sur les chekbox
    var listcheckbox = document.getElementsByClassName("checkbox");
    for(var i = 0;i < listcheckbox.length;i++){
    	var button = listcheckbox[i].getElementsByTagName("input");
    	switch(i){
    		case 0:
    		   $(button).click(function(){sortByStream(1,1);});
    		 break;
    		case 1:
    		 $(button).click(function(){sortByStream(1,0);});
    		 break;
    		case 2:
    		 $(button).click(function(){sortByPrice(2,1);});
    		 break;
    		case 3:
    		   $(button).click(function(){sortByPrice(2,0);});
    		 break;
    	}    	
    }

  $("#contenu-subscribe").css("display","none");
    //Evenements clic sur les inputs popup connexion
    $(".popup-connexion .row-popup p:nth-child(1)").on("click",function(){
        $(".popup-connexion .row-popup p:nth-child(1)").css({"background" : "#FFFFFF","color" :"#333"});
        $(".popup-connexion .row-popup p:nth-child(2)").css({"background" :"#3399CC","color" : "#FFFFFF"});
        $("#contenu-connect").css("display","block");
        $("#contenu-subscribe").css("display","none");
    })

    $(".popup-connexion .row-popup p:nth-child(2)").on("click",function(){
         $(".popup-connexion .row-popup p:nth-child(2)").css({"background" : "#FFFFFF", "color":"#333"});
         $(".popup-connexion .row-popup p:nth-child(1)").css({"background" : "#3399CC","color":"#FFFFFF"});
          $("#contenu-connect").css("display","none");
        $("#contenu-subscribe").css("display","block");
    })

    //Mise en place d'évenelents sur les chois des tries

      $("#ville").on("change", function(){ sortByTown($("#ville").val());});
      $("#langue").on("change", function(){ sortByLanguage($("#langue").val());});
      $("#date").on("change", function(){ sortByDate($("#date").val());});

       $(".close").on("click",function(){$(".popup-connexion").css("display","none");});

});


/********************** Class modélisant une conférence **********************/

    function Conference(nom,date,ville,stream,langue,prix,link)
    {
      this.nom = nom;
      this.date = date;
      this.ville = ville;
      this.stream = stream;
      this.langue = langue;
      this.prix = prix;
      this.lien = link;
      this.MyMonth = function(){
           return new Date(this.date).getMonth()+1;
      }

      this.ImInFrench = function(){
        var return_langue = true;
        if(this.langue != "Fr")
          return_langue = false;

        return return_langue;
    }

    this.getDateFrenchFormat = function(){
      var date =  new Date(this.date);
      return  date.getDay()+"/"+this.MyMonth()+"/"+date.getFullYear();
    }

    this.ImFree = function(){
        var return_free = true;
        if(this.prix > 0)
          return_free = false;

        return return_free;
    }

    this.getMyTown = function(){
        return this.ville;
    }

    this.getName = function(){
      return this.nom;
    }

    this.getDate = function(){
        return this.date;
    };

    this.getLangue = function(){
      return this.langue;
    }

    this.getPrix = function(){
      return this.prix;
    }

  }

              /************* TESTS ***********************/

    function RemplirCollection(){

       LesConferences.push(new Conference("BDX-IO", "2015/10/16","Bordeaux","Oui","Fr",0,"conference.html"));
       LesConferences.push(new Conference("PARIS WEB", "2015/10/03","Paris","Oui","Fr",25,"parisweb.html"));
       LesConferences.push(new Conference("FRENCH WEB DAY", "2015/11/26","Toulouse","Oui","Fr",0,"frenchweb.html"));
       LesConferences.push(new Conference("TECH DAYS", "2015/02/10","Paris","Oui","Fr",25,"techday.html"));

       ReturnLikeHtmlLines(1);
}

/*******************************************************/

/******************** Méthodes traitant du DOM ****************************/



function CreatePagination() {  
  var nb_page = Math.ceil((LesConferences.length/5));
  var node_pagination = document.getElementsByClassName("pagination");
   for(var indentation = 1; indentation < nb_page; indentation++)
   {
    var page = document.createElement("li");
    page.setAttribute("id",(indentation+1));
    var link = document.createElement("a");
    link.textContent = (indentation+1).toString();
    link.setAttribute("href","javascript:Gererpagination("+(indentation+1)+","+(indentation+1)+")");
    page.appendChild(link);
    node_pagination[0].appendChild(page);
  }
}

function ReturnLikeHtmlLines(collection){
  var start = 0;
  var end = 0;
     if(page == 1)
      end = 4;
     else
     {
      start = ((5*page)-5);
      end = (5*page)-1;
     }

   var node = document.getElementById("contenu");
   if(node != null){
   if(node.getElementsByTagName("tr").length > 0){
    Supprimertouslesnoeuds("table");}    
    CreateHtmlLines(start,end,node,collection);
  }
}


function CreateHtmlLines(start,end,node,collection){

    var thecollection = new Array();

    if(collection == 1)
      thecollection = LesConferences;
    else
      thecollection = newCollection;

      Conference.prototype.getLien = function(){
        return this.lien;
      }

   for(var i = start;i <= end;i++) {
      if(thecollection[i] != undefined)
      {
       var line = document.createElement("tr");
       line.setAttribute("onClick","window.open('"+thecollection[i].getLien()+"')");
       var cell1 = document.createElement("td");
       var cell2 = document.createElement("td");
       var cell3 = document.createElement("td");
       var cell4 = document.createElement("td");
       cell1.appendChild(document.createTextNode(thecollection[i].nom)); //.toLowerCase().charAt(0).toUpperCase()+ LesConferences[i].nom.substring(1).toLowerCase()
       cell2.appendChild(document.createTextNode(thecollection[i].getDateFrenchFormat()));
       cell3.appendChild(document.createTextNode(thecollection[i].ville));
       cell4.appendChild(document.createTextNode(thecollection[i].stream));
     
      line.appendChild(cell1);
      line.appendChild(cell2);
      line.appendChild(cell3);
      line.appendChild(cell4);
      node.appendChild(line);
    }
  }
}

function Supprimertouslesnoeuds(noeud){
    arrayLignes = document.getElementById(noeud);
    for(var i = 0;i < arrayLignes.childNodes.length;i++)
    {
    	if(arrayLignes.rows.length > 1)
    	{
    	   arrayLignes.deleteRow(1);
    	}
    }
}	

function Gererpagination(pageparam,button){
	if(LesConferences.length >= (5*pageparam)-4)
	{
    page = pageparam;
		ReturnLikeHtmlLines(1);
	}
    changerclass(button);
}

function GererpaginationSort(pageparam,button){
  if(newCollection.length >= (5*page)-4){
     page = pageparam;
    ReturnLikeHtmlLines(2);  
  }

  changerclass(button);
}

function paginationForSort(){
  var nb_page = Math.ceil((newCollection.length/5));

   var node_pagination = document.getElementsByClassName("pagination");
   for(var indentation = 0; indentation < nb_page; indentation++)
   {
    var page = document.createElement("li");
    if(indentation==0){
        page.setAttribute("class","active");
    }
    page.setAttribute("id",(indentation+1));
    var link = document.createElement("a");
    link.textContent = (indentation+1).toString();
    link.setAttribute("href","javascript:GererpaginationSort("+(indentation+1)+","+(indentation+1)+")");
    page.appendChild(link);
    node_pagination[0].appendChild(page);
  }
}

function Supprimernavigation(){
    var noeud = document.getElementsByClassName("pagination");
    for(var i = 0;i <= (noeud[0].childElementCount+1);i++)
    {
      if(noeud[0].childElementCount > 1)
         noeud[0].removeChild(noeud[0].children[1]); 
    }
} 

function changerclass(button){
	var bloc = document.getElementsByClassName("pagination");
  if(bloc.length > 0)
  {
     for(var i=0; i < bloc[0].childNodes.length;i++)
     	$(bloc[0].children[i]).removeClass("active");
	var change = document.getElementById(button);
    $(change).addClass("active");
  }
}

/**********************************************************************************/


/******************* Méthodes de trie **********************/
function Sort(kind_of_sorts,actived){ 
  //to_do
}

function sortByStream(type,actived){
	/*Supprimertouslesnoeuds("table");
	*/
}

function sortByPrice(type,actived){
	/*Supprimertouslesnoeuds("table");
	*/

  //to_do
}

function sortByLanguage(langue){
  if($("#ville").val() != null)
  {
    if(compteur==0){
       for(var i =0;i < newCollection.length;i++){
          oldCollection[i] = newCollection[i];
       }

        compteur++;}
       else{
         newCollection = new Array();
        for(var i =0;i < oldCollection.length;i++){
          newCollection[i] = oldCollection[i];
       } 
    }
    
    for(var i = 0; i < newCollection.length;i++){
      if(newCollection[i].getLangue() != langue)
      {
        newCollection.splice(i,1);
        i--;
      }
    }
  }

  Supprimertouslesnoeuds("contenu");
  Supprimernavigation("pagination");
  paginationForSort();
  ReturnLikeHtmlLines(2);
}

function sortByDate(date){
  if($("#langue").val() != null)
  {
    if(compteurdate==0){
     for(var i =0;i < newCollection.length;i++){
          oldCollection[i] = newCollection[i];}
          compteurdate++;
    }
    else
    {
       newCollection = new Array();
        for(var i =0;i < oldCollection.length;i++){
          newCollection[i] = oldCollection[i];
    }

    for (var i = 0; i < newCollection.length; i++) {
      if(newCollection[i].MyMonth() != date)
      {
         newCollection.splice(i,1);
         i--;
      }
    }
  }}

  Supprimertouslesnoeuds("contenu");
  Supprimernavigation("pagination");
  paginationForSort();
  ReturnLikeHtmlLines(2);
 }

function sortByTown(ville){

  var node = document.getElementById("contenu");
   newCollection = new Array();

  for(var i = 0; i < LesConferences.length;i++){
    if(LesConferences[i].getMyTown() == ville ){
        newCollection.push(LesConferences[i]);
      }
  }

  Supprimertouslesnoeuds("table");
  Supprimernavigation("pagination");
  paginationForSort();
  ReturnLikeHtmlLines(2);
}

function sortByCategorie(){
  //to_do
}


/*****************************************************/

/************************ JS for popup boxs ********************************/

    function toggle_visibility(id) {
       var e = document.getElementById(id);
       $(e).css("display",'block');
       if(parseInt($(e).css("left"),10) >= 0 ) {
          $(e).animate({"left": "-25%"}, "slow").queue(function(){
          $(e).css("display","none"); $(e).dequeue(); });   }
       else{
         $(e).css("display","block").queue(function(){
          $(e).animate({"left": "0%"}, "slow" ).dequeue();          
    });
  } 
}

   function cacher(){
    var e = document.getElementById("popup-box");
   	  $(e).animate({ "left": "-25%" }, "slow" ).queue( function(){
   	  	$(e).css("display","none").dequeue();
   	  });
   }


   function cacher_id(){
   	  var e = document.getElementById("popup-box-identification");
   		$(e).animate({ "left": "-13%" }, "slow" ).queue( function(){
   	  	$(e).css("display","none");
   	  	$(e).dequeue();
   	  });
   }

   function toggle_visibility_id(id) {
   	var e = document.getElementById(id);
       $(e).css("display","block");
       if(parseInt($(e).css("left"), 10) > 16  || isNaN(parseInt($(e).css("left"), 10))) {
          $(e).animate({"left": "-13%"}, "slow"); }
       else{
         $(e).animate({"left": "+13%"}, "slow" ).queue(function(){
         	$(e).css("display","none"); 
         $(e).dequeue(); 
     });          	      
    }
   }


   function connect(){
    cacher();
    $(".popup-connexion").css("display",'block');
    }


/*********************************************************************/